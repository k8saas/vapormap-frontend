FROM nginx

WORKDIR /usr/src/app

RUN apt-get update && apt-get install gettext-base && apt install -y git
RUN git clone https://gitlab.com/vapormap/vapormap-app.git

WORKDIR /usr/src/app/vapormap-app/frontend

EXPOSE 80

CMD envsubst '${VAPORMAP_URL_SERVERNAME},${VAPORMAP_URL_PORT},${VAPORMAP_FRONTEND_ROOT}' < nginx.conf.template > vapormap.conf ;\
    envsubst '${VAPORMAP_BACKEND},${VAPORMAP_BACKEND_PORT}' < config.json.template > config.json ;\
    cp vapormap.conf /etc/nginx/confd;\
    cp -r . /usr/share/nginx/html; \
    nginx -g "daemon off;";








